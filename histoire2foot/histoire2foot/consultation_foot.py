import histoire2foot

def afficher_option(options):
    print("Veuillez choisir une option : ")
    for i in range(len(options)):
        print(str(i+1)+" --> ",options[i])


def choisir_options(options):
    cond = False
    while not cond :
        try: 
            afficher_option(options)
            rep = int(input("Entrez un nombre entier entre 1 et 4 : \n "))
            if rep > 0 and rep < 10 :
                cond = True
            else:
                print("Votre reponse doit être entre 1 et 4")
        except ValueError:
            print("Votre reponse n'est pas un entier")
    return rep


def afficher_matchs(liste_match):
    if liste_match == []:
        print("Aucun match enregistré")
    else:
        for match in liste_match:
            if match[-1] == True:
                neutre = "Oui"
            else:
                neutre = "Non"
            print("Date:",match[0],"Equipe à domicile:",match[1],"Equipe exterieur :",match[2],"Score de l'equipe à domicile :",match[3],"Score de l'equipe exxterieur :",match[4],"Nom du tournoi:",match[5],"Ville:",match[6],"Pays:",match[7],"Neutre:",neutre )

def contient_equipe(liste_matchs,equipe) :
    for match in liste_matchs :
        if equipe in liste_matchs[0] :
            return True
    return False

def contient_ville(liste_matchs,ville) :
    for match in liste_matchs :
        if ville in liste_matchs[0] :
            return True
    return False

def programme_principal():
    liste_options = ["Ajouter des matchs via un fichier csv",
    "Afficher la listes des matchs ",
    "Stat équipe",
    "Première victoire",
    "Nombre de but moyen sur une compétition",
    "Liste des match sur une ville",
    "Match avec le plus gros écart",
    "Le plus de victoire sans défaite",
    "Quitter le menu"]
    liste_matchs = []
    while True:
        rep = choisir_options(liste_options)

        if rep == 1:
            print("Vous avez choisi l'option :",liste_options[rep-1])
            nom_fic = input("entrer le nom du fichier: \n")
            liste_matchs = histoire2foot.fusionner_matchs(liste_matchs,histoire2foot.charger_matchs(nom_fic))
            print(liste_matchs)
            print("la liste des maths a ete mise a jour")
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 2:
            print("Vous avez choisi l'option :",liste_options[rep-1])
            afficher_matchs(liste_matchs)
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 3:
            print("Vous avez choisi l'option :",liste_options[rep-1])
            equipefirstvic = input("Entrer l'equipe souhaité : ")
            if liste_matchs == [] :
                print("La liste est vide veuillez l'initialiser")
            elif contient_equipe(liste_matchs,equipefirstvic) :
                res = histoire2foot.premiere_victoire(liste_matchs,equipefirstvic)
                print("La première victoire de",equipefirstvic,"est",res)
            else :
                print("L'équipe n'existe pas veuillez réessayez")
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 4 :
            print("Vous avez choisi l'option :",liste_options[rep-1])
            equipe = input("Entrer l'equipe souhaité : ")
            if liste_matchs == [] :
                print("La liste est vide veuillez l'initialiser")
            elif contient_equipe(liste_matchs,equipe) :
                res = histoire2foot.resultats_equipe(liste_matchs,equipe)
                print("Les statistique de l'équipe",equipe,"est (vic/def/nul)",res)
            else :
                print("L'équipe n'existe pas !")
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 5 :
            print("Vous avez choisi l'option :",liste_options[rep-1])
            nom_compete = input("Veuillez mentionner la competition souhaiter ")
            res = histoire2foot.nombre_moyen_buts(liste_matchs,nom_compete)
            if liste_matchs == [] :
                print("La liste est vide veuillez l'initialiser")
            elif res == None :
                print("La compétition n'existe pas !")
            else :
                print("Pour la competition",nom_compete,"Le nombre de but moyen est de :",res)
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 6 :
            print("Vous avez choisi l'option :",liste_options[rep-1])
            ville = input("Quelle est la ville que vous souhaitez ? ")
            if liste_matchs == [] :
                print("La liste est vide veuillez l'initialiser")
            if contient_ville(liste_matchs,ville) :
                res = histoire2foot.matchs_ville(liste_matchs,ville)
                print("Voici la liste qui se sont dérouler à",ville,res)
            else :
                print("La ville n'existe pas !")
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 7 :
            if liste_matchs == [] :
                print("La liste est vide veuillez l'initialiser")
            print("Vous avez choisi l'option :",liste_options[rep-1])
            res = histoire2foot.plus_gros_scores(liste_matchs)
            print ("Voici la liste des matchs avec laquelle l'écart est le plus grand",res)
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 8:
            print("Vous avez choisi l'option :",liste_options[rep-1])
            equipe = input("Quel est l'équipe que vous souhaiter ? ")
            if liste_matchs == [] :
                print("La liste est vide veuillez l'initialiser")
            elif contient_equipe(liste_matchs,equipe) :
                res = histoire2foot.nb_matchs_sans_defaites(liste_matchs,equipe)
                print("Voici le plus grand nombre de victoire sans défaite de l'équipe",equipe,"est :",res)
            else :
                print("Votre équipe n'existe pas !")
            entrer = input("Appuyer sur la touche entrer pour continuer \n")
        elif rep == 9:
            print("Vous avez choisi l'option :",liste_options[rep-1])
            print("fait")
            break


programme_principal()



    

